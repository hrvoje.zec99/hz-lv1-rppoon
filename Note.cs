using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class Note
    {
        //2.zadatak
        private string text;
        private string authorName;
        private int importance;

        public string getText() { return this.text; }
        public string getAuthorName() { return this.authorName; }
        public int getImportance() { return this.importance; }


        public void setText(string text) { this.text = text; }
        public void setAuthorName(string authorName) { this.authorName = authorName; }
        public void setImportance(int importance) { this.importance = importance; }




        //3.zadatak


        public Note()
        {
            this.text = "Danas pada snijeg.";
            this.authorName = "Hrvoje";
            this.importance = 0;
        }

        public Note(string text, string authorName, int importance)
        {
            this.text = text;
            this.authorName = authorName;
            this.importance = importance;
        }
        public Note(string authorName, int importance)
        {
            this.text = "Hrvoje";
            this.authorName = authorName;
            this.importance = importance;
        }




        //4.zadatak
        public String Text
        {
            get { return text; }
            set { this.text = value; }
        }

        public String AuthorName
        {
            get { return authorName; }
        }

        public int Importance
        {
            get { return importance; }
            set { this.importance = value; }
        }
        //5.zadatak
        public override string ToString()
        {
            return (this.authorName + "," + this.importance);
        }
    }
}
